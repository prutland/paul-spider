#!/usr/bin/env node

'use strict';

// My Requires
const _ = require('lodash');
const args = require('yargs').argv;
const fs = require('fs');
const yaml = require('js-yaml');
const cheerio = require('cheerio');



// My funsctions
const getHTML = function(url) {

  // return new pending promise
  return new Promise((resolve, reject) => {

    // select http or https module, depending on reqested url
    const lib = url.startsWith('https') ? require('https') : require('http');

      try {
        const request = lib.get(url, (response) => {        // handle http errors
        if (response.statusCode < 200 || response.statusCode > 299) {
           reject (new Error('Failed to load page, status code: ' + response.statusCode));
         }
        // temporary data holder
        const body = [];
        // on every content chunk, push it to the data array
        response.on('data', (chunk) => body.push(chunk));
        // we are done, resolve promise with those joined chunks
        response.on('end', () => resolve(body.join('')));
      });

      // handle connection errors of the request
     request.on('error', (err) => reject(new Error("There is a problem with the url you are trying to scrape. "+err)) );

    } catch (e) {
      throw new Error ("There is a problem with the url you are scraping. Make sure it  starts http:// or https://");
    }

  })
};

const parseHTML = function(html) {

  return new Promise((resolve, reject) => {
    try {
      //let html_object = htmlparser.parseDOM(html);
      resolve(cheerio.load(html));
    } catch (e) {
      reject(new Error ("Problem parsing the HTML"));
    }

  })
}

const extractInfo = function(parsed_html,yaml) {

  return new Promise((resolve, reject) => {
    try {

       let scrapeResults = {};

       for (let title in yaml) {

         scrapeResults[title] = [];

           parsed_html('html').find(yaml[title].path.join(" > ")).each(function(index,element) {
             if (!_.isUndefined(element.attribs) && !_.isEmpty(element.attribs)) {
               if (!_.isUndefined(element.attribs.href)) {
                  scrapeResults[title].push(element.attribs.href);
               }
             } else {

              if (!_.isUndefined(element.children) && _.isArray(element.children) && !_.isEmpty(element.children)) {
                for (let child in element.children) {
                    scrapeResults[title].push(element.children[child].data);
                }
              }

            }
           });
       }

       resolve(scrapeResults);

    } catch (e) {
      reject(new Error ("Problem Extracting the information"));
    }

  })
}


async function letsGetThisPartyStarted(yaml,url) {

  try {
       const html = await getHTML(url);
       const parsed_html = await parseHTML(html);
       const done = await extractInfo(parsed_html,yaml);
       return done;
   } catch (e){
       console.log(e);
   }
}


// You must have these arguments
if ((_.isUndefined(args.path) &&  _.isEmpty(args.path)) || (_.isUndefined(args.url) &&  _.isEmpty(args.url))) {
   console.log("format ./spider.js --path <filepath> --url <url to scrape>")
   process.exit();
}


// Validate YAML File
let path = args.path;
let doc = null;

// Get document, or throw exception on error
try {
  doc = yaml.safeLoad(fs.readFileSync(path,'utf8'));
} catch (e) {
  console.log("Invalid yaml File - file may not exist or is not valid yaml");
  process.exit();
}

// If you get this far with exiting you are going well
let url = args.url;

letsGetThisPartyStarted(doc,url).then (results => {
   console.log(results);
});
