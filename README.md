# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Node Modules I Used ###

* yargs - For getting those scurvy arguments from the command line
* js-yaml - For processing a YAML into an object
* http and https - for getting the HTML page
* cheerrio - for parsing that HTML into something that could be used to scraped

### How to run the thing

./spider --path <path to yaml file> --url <url of page to be scraped>

./spider --path example.yaml --url http://www.scoot.contact-us

#### Example for YAML file for www.scoot.contact-us

```javascript
TitleTag:
  path:
    - head
    - title
HeadLink:
  path:
    - head
    - link
BodyLink:
  path:
    - body
    - div.em-footer
    - div.em-footer-mt
    - div.em-navbar-footer
    - div.container
    - div.navbar-collapse
    - ul.em-nav
    - li
    - a
```

### Output from script

```javascript
{ TitleTag: [ 'Scoot the UK Business Finder' ],
  HeadLink: 
   [ 'http://fonts.googleapis.com/css?family=Varela+Round',
     '/img/favicon-sc.ico',
     '/css/bootstrap.min.css',
     'css/network-styles.css',
     '/css/em-theme-sc.css' ],
  BodyLink: 
   [ '/network',
     '/about-us',
     '/privacy-policy',
     '/cookie-policy',
     '/terms-conditions',
     '/contact-us',
     '/add-listing' ] }
```
